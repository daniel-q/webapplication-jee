package bo.com.ypfbandina.webapplication.services.application.internal;

import bo.com.ypfbandina.webapplication.services.application.FeatureOne;
import bo.com.ypfbandina.webapplication.services.domain.interfaces.service.HelloWorldService;

import javax.inject.Inject;

public class FeatureOneImpl implements FeatureOne {

    private HelloWorldService helloWorldService;

    @Inject
    public FeatureOneImpl(HelloWorldService helloWorldService) {
        this.helloWorldService = helloWorldService;
    }

    public String Greeting() {
        return helloWorldService.Greeting();
    }
}
