package bo.com.ypfbandina.webapplication.rest;

import bo.com.ypfbandina.webapplication.services.application.FeatureOne;
import bo.com.ypfbandina.webapplication.services.domain.interfaces.service.HelloWorldService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/hello")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";CHARSET=UTF-8")
public class HelloWorldResource {

    @Inject
    private FeatureOne featureOne;

    @GET
    @Path("/greeting")
    public Response Greeting() {
        return Response.ok(featureOne.Greeting()).build();
    }
}

