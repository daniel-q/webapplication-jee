package bo.com.ypfbandina.webapplication.services.infrastructure.repository;

import bo.com.ypfbandina.webapplication.services.domain.interfaces.repository.UserAccountRepository;
import bo.com.ypfbandina.webapplication.services.domain.model.UserAccount;
import org.jinq.jpa.JinqJPAStreamProvider;
import org.jinq.orm.stream.JinqStream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class UserAccountRepositoryImpl implements UserAccountRepository {

    public UserAccount UserAccountByName(String userName) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("webapppu");
        EntityManager em = emf.createEntityManager();
        JinqJPAStreamProvider streams = new JinqJPAStreamProvider(emf);

        JinqStream<UserAccount> users = streams.streamAll(em, UserAccount.class);

        List<String> userNames = users
                .where(u -> u.getName().equals("daniel"))
                .select(UserAccount::getLastName)
                .toList();

        return null;
    }
}
