package bo.com.ypfbandina.webapplication.services.infrastructure.repository;

import java.util.List;

public class GenericRepository<T> {

    public List<T> query() {
        return null;
    }

    public T get() {
        return null;
    }

    public List<T> getAll() {
        return null;
    }

    public void create(T entity) {

    }

    public void update(T entity) {

    }

    public void remove(int id) {

    }
}
