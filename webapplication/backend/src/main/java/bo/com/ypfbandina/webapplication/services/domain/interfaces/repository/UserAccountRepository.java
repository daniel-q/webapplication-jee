package bo.com.ypfbandina.webapplication.services.domain.interfaces.repository;

import bo.com.ypfbandina.webapplication.services.domain.model.UserAccount;

public interface UserAccountRepository {

    UserAccount UserAccountByName(String userName);
}
