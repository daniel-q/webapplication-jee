package bo.com.ypfbandina.webapplication.services.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;

@Getter
@Setter
public class UserAccount {

    @Id
    private long userId;

    private String name;

    private String lastName;
}
