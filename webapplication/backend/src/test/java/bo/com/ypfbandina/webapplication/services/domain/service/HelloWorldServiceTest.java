package bo.com.ypfbandina.webapplication.services.domain.service;


import org.apache.logging.log4j.core.util.Assert;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class HelloWorldServiceTest {

    @DisplayName("Single test successful")
    @Test
    void testSingleSuccessTest() {
        Assert.isNonEmpty("hola");
    }

    @Test
    @Disabled("Not implemented yet")
    void testShowSomething() {
        Assert.isNonEmpty("hola");
    }
}
